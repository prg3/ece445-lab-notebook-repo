**Design Check (2/22/2022)**:
-Focus on high level requirements (use quantitative measurements)
-Make tolerance analysis more detailed (incomplete) 

**Design Review (3/2/2022)**:
-Make higher level requirements more general (1. Does it actually clean the panels 2.Make sure we are actually using less power than saving, etc…)
-Add numbers to clearing subsystem requirements 
-Cost needs to be modified for overhead requirements 
-Consider what other objects could end up on panels; Perhaps could use a PIR sensor to distinguish between actual debris and other objects?? 

**TA Meeting (2/21/2022)**:
-Need to add more to subsystem requirements and tolerances analysis
-Add pictures for visual aid to make it more detailed 
-Provide quantitative information about circuit elements
-TODO for me: Requirements and Verification for Software portion, Cost Breakdown, Upload datasheets for microcontroller to google drive 

**PCB Review (3/1/2022)**:
-Delete Footprints and add breakout points using connectors
-ESP chip vs ESP Devkit ??
-Add labels for components 
-Reduce Board Area (should be 100x100)

**TA Meeting (3/7/2022)**:
-Purchase orders in before spring break
-Solar panel access from professor
-Will start programming microcontroller (Arduino test bench) to analyze current readings from sensor

**Lab Session (3/10/22)**: 
-Worked on getting current readings from hall effect sensor
-Used Arduino UNO R3 as microcontroller test since Arduino IDE can be used to program our ESP microcontroller 
-Obtained readings from wire into INPUT PIN on Arduino and printed the value(s) in a continuous loop 
-Need to understand the scale of the readings since 0 amps is approximately read as 500 by the microcontroller 

![ESP32_Analog_Readings__1_](/uploads/d229114691ea4c9845a4ab2bd5d92a80/ESP32_Analog_Readings__1_.JPG)

Sources: https://www.arduino.cc/reference/en/

**Lab Session (3/24/22)**:
-Want to test relays and Wifi Connection with microcontroller
-Could not find USB cable to actually flash code and power the microcontroller
-Looked through Arduino Documentation to understand Wifi library and digitalWrite() capability for sending high voltage signals to relays 

Sources: https://www.arduino.cc/reference/en/

**Lab Session (3/25/22)**:
-Worked on Microcontroller Wifi Configuration and providing power to the relays on Arduino test bench 
-Utilized ESP Core Wifi Library to treat the microcontroller; Set microcontroller in station mode so it can connect to an access point; fed SSID and password to microcontroller; - Issue: cannot connect to campus Wifi due to authenfication issues Workaround: Use Wifi hotspot via phone 
-Wrote code to oscillate between HIGH and LOW voltage signals to send to the relays (open and close switch) on Arduino test bench

WiFi Code: 

![WiFi_Configuration](/uploads/0a998cddbd0cfb7ca08cafbdb4eb8cb4/WiFi_Configuration.JPG)

Relay Code: 

![Relay_Code__1_](/uploads/28c4844c8dd167e3d232ebdf23eb4ee2/Relay_Code__1_.JPG)

Relay Pin is set to Pin A4 on the on the Arduino test bench

Sources: https://www.arduino.cc/reference/en/, https://randomnerdtutorials.com/esp32-useful-wi-fi-functions-arduino/

**Lab Session (3/29/22)**:
-Used Arduino code from previous session to activate relays using Pin 4 on the microcontroller (worked well!)
-Attempted to read hall sensor readings (voltage output) but values were wildly fluctuating 
-Used multimeter to produce an IV characteristic graph 

![Hall_Sensor_IV-graph__1_](/uploads/34ddb727202fcabcba4d27bae1357d7b/Hall_Sensor_IV-graph__1_.JPG)

The approximately linear slope drives us to think scaling factor can be used to convert quantized values from A/D converter 

**Lab Session (3/31/22)**: 
-Found scaling formula in arduino documentation ( (raw_value *3.3) / 1024)
-implemented scaling in arduino code and took hall sensor readings on the arduino 

![Hall_Sensor_Readings__1_](/uploads/36d12b8b4302e4cac0764ec537b62f7f/Hall_Sensor_Readings__1_.JPG)

Sources: https://www.arduino.cc/reference/en/

**Independent Session (4/2/22)**: 
-Implemented Email notification from microcontroller using ESPMailClient library 
-Utilized SMTP (Simple Mail Transfer Protocol) which is an internet standard for email transmission; ESP32 connects to STMP server 

![Email_setup__1_](/uploads/1f78f8e65a3c849719548407da5fdd25/Email_setup__1_.JPG)

-declare SMTP host server
-declare SMTP server port
-declare sender email address, app password, and recepient email address
-create SMTP session object 

![Email_setup__2_](/uploads/f0be40ea932c3207a745c9ac678ba333/Email_setup__2_.JPG)

-create mail session object
-bind host, port, email, and password to object
-create message object 
-bind sender name, email address, subject name of email, and recepient email address 

![Email_setup__3_](/uploads/4fa5b83efdd21170ada8bf5cc4680e2b/Email_setup__3_.JPG)

-Use HTML encoding to write message; convert to string using ASCII
-Makes attempt to connect to SMTP server
-Attempts to send mail and then closes session afterwards (unless error)

Sources: https://randomnerdtutorials.com/esp32-send-email-smtp-server-arduino-ide/

Individual Session (4/3/22):

-implemented weather API contact and information retrieval 

![weather_api_code](/uploads/db6d67f24b20b1da9635959fbdc7ceca/weather_api_code.JPG)

-utilizies HTTP GET Request to contact API
-information received in form of JSON structure
-parsed JSON structure to print information about temperature and cloud coverage 

**Lab Session (4/5/22 -> 4/7/22): **

-Implemented communication link between main microcontroller and current sensor microcontroller (ESP-Now)

![receiver_esp_now_code_1](/uploads/751280b5c9a1fd0d2e9b972984f285e4/receiver_esp_now_code_1.JPG)


-first function triggers upon data being sent from main microcontroller 
-second function triggers upon data being received from current meter microcontroller

![transmitter_esp_now_code_1](/uploads/697cc1725c3584c4bf01fc62f307bdc2/transmitter_esp_now_code_1.JPG)

-first function triggers upon data being sent from current sensor microcontroller
-second function triggers upon data being received from main microcontroller

-Receiver sends message to current meter if cleaning is starting or not
-Transmitter sends message to main microcontroller about degradation statistics 

-Code worked pretty well on initial test attempts (no major debugging was required)

**Lab Session (4/8/22):**

-Tested ESP32 microcontroller with hall sensor output
-Direct connection from 3.3V to ground gave accurate reading (no noise; stable)
-added resistance and excess noise became issue (fluctuation)
-Since current drop happens from 4.2A to 2.0A, the voltage drop should be in the hundredths, but change is pretty small and it fluctuates
-Tested with 10K resistor, but results did not vary signficiantly 

**Lab Session (4/13/122):**

-reading directly from output pin led to scaled down version of actual output
-This scaling was not consistent so reading from pin is not an option
-Did additional research and found the possible addition of 100nF bypass capacitors from output to ground 

**Lab Session (4/14/22 -> 4/18/22):** 

-Same Setup with measuring voltage drop across 1K resistor 
-Utilized bypass capacitors and noticed noise attenuation 
-Tested tuning of microcontroller reference voltage (Vref) with true voltage of 1.60V

![Vref_config_1](/uploads/f09cb89c01d4f45886b8a62e7a502765/Vref_config_1.JPG)

-Tuning Vref and addition of bypass capacitors restricts the fluctuation very well compared to initial experiments
-When test voltage was decreased to 1.55V, the readings were in the range of 1.55-1.56V with Vref-1070mV 
-0.01 to 0.02 fluctuation is usable but trying for better

**Lab Session (4/19/22-4/21/22):**

-Created code flowcharts concerning data collection and analysis algorithms for transmitter and receiver 

![Transmitter_code_flowchart](/uploads/e9d36c569e08a1d00473ddf51df63a51/Transmitter_code_flowchart.JPG)

-Important aspects (transmitter flowchart): gathering large number of samples, using max outlier (which is tuned to be close to actual value), detecting degradation if present, if degradation is present check to see if there is holding (so not just a fluctuation)

![Receiver_code_flowchart](/uploads/bd5a97a09161315ce73a3f28391ae593/Receiver_code_flowchart.JPG)

-Important aspects (receiver flowchart): If notification received from transmitter weather API gets invoked, if significant cloudiness then flags are set for cleaning, if flags set for cleaning high voltage signals are set to the relay modules for sprayer and wiper 

**Lab Session (4/23/22 -> 4/26/22):** 

-Worked on transmitter code for decoding voltage information from hall sensor 

![max_value_code](/uploads/1f1a07cb87418db6be33f17a3415c8cb/max_value_code.JPG)

-if number of samples needed for checking has been reached, find max value (outlier) in those samples
-outlier will not be too far from actual voltage reading due to attenuation techniques 


![degradation_check_code](/uploads/106ebfc0ff6168faa2a336c8ea862725/degradation_check_code.JPG)

-scan array and check if significant dip occurs anywhere in the samples taken 

![degradation_hold_1](/uploads/9e88eb49a50f62f91605c5e9915b2e36/degradation_hold_1.JPG)

-checks if degradation holds
-if voltage bounces back upwards past a certain threshold, then treated as a fluctuation

![degradation_hold_2](/uploads/176183091d18496788eab490f470e519/degradation_hold_2.JPG)

-if degradation held for a specified number of samples, then transmitter notifies receiver of possible cleaning
-array is cleared to continue scanning new samples afterwards 

![degradation_hold_3](/uploads/2d17bdf6ffa5859dd5505a739261aa38/degradation_hold_3.JPG)

-code to actually transmit notification from transmitter to receiver 
-mac address is specified in initial setup 
-since two way communication channel, delay of transmitter to collect data is based on signal sent back from receiver 





